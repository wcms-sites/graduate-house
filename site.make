core = 7.x
api = 2
; uw_ct_food_outlet
projects[uw_ct_food_outlet][type] = "module"
projects[uw_ct_food_outlet][download][type] = "git"
projects[uw_ct_food_outlet][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_food_outlet.git"
projects[uw_ct_food_outlet][download][tag] = "7.x-1.13"
projects[uw_ct_food_outlet][subdir] = ""
